
add_library(libana OBJECT
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/audio_network_adaptor_config.cc
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/audio_network_adaptor_impl.cc
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/bitrate_controller.cc
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/channel_controller.cc
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/controller.cc
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/controller_manager.cc
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/debug_dump_writer.cc
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/dtx_controller.cc
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/event_log_writer.cc
		${webrtc_source_path}/modules/audio_coding/audio_network_adaptor/frame_length_controller.cc
)

target_compile_options(libana PRIVATE
)

target_compile_definitions(libana PRIVATE
)

target_include_directories(libana PRIVATE

)
