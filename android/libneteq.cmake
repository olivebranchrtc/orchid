
add_library(libneteq OBJECT
		${webrtc_source_path}/modules/audio_coding/neteq/accelerate.cc
		${webrtc_source_path}/modules/audio_coding/neteq/audio_multi_vector.cc
		${webrtc_source_path}/modules/audio_coding/neteq/audio_vector.cc
		${webrtc_source_path}/modules/audio_coding/neteq/background_noise.cc
		${webrtc_source_path}/modules/audio_coding/neteq/buffer_level_filter.cc
		${webrtc_source_path}/modules/audio_coding/neteq/comfort_noise.cc
		${webrtc_source_path}/modules/audio_coding/neteq/cross_correlation.cc
		${webrtc_source_path}/modules/audio_coding/neteq/decision_logic.cc
		${webrtc_source_path}/modules/audio_coding/neteq/decoder_database.cc
		${webrtc_source_path}/modules/audio_coding/neteq/delay_manager.cc
		${webrtc_source_path}/modules/audio_coding/neteq/delay_peak_detector.cc
		${webrtc_source_path}/modules/audio_coding/neteq/dsp_helper.cc
		${webrtc_source_path}/modules/audio_coding/neteq/expand.cc
		${webrtc_source_path}/modules/audio_coding/neteq/expand_uma_logger.cc
		${webrtc_source_path}/modules/audio_coding/neteq/histogram.cc
		${webrtc_source_path}/modules/audio_coding/neteq/merge.cc
		${webrtc_source_path}/modules/audio_coding/neteq/nack_tracker.cc
		${webrtc_source_path}/modules/audio_coding/neteq/neteq_impl.cc
		${webrtc_source_path}/modules/audio_coding/neteq/neteq.cc
		${webrtc_source_path}/modules/audio_coding/neteq/normal.cc
		${webrtc_source_path}/modules/audio_coding/neteq/packet.cc
		${webrtc_source_path}/modules/audio_coding/neteq/packet_buffer.cc
		${webrtc_source_path}/modules/audio_coding/neteq/post_decode_vad.cc
		${webrtc_source_path}/modules/audio_coding/neteq/preemptive_expand.cc
		${webrtc_source_path}/modules/audio_coding/neteq/random_vector.cc
		${webrtc_source_path}/modules/audio_coding/neteq/red_payload_splitter.cc
		${webrtc_source_path}/modules/audio_coding/neteq/statistics_calculator.cc
		${webrtc_source_path}/modules/audio_coding/neteq/sync_buffer.cc
		${webrtc_source_path}/modules/audio_coding/neteq/time_stretch.cc
		${webrtc_source_path}/modules/audio_coding/neteq/tick_timer.cc
		${webrtc_source_path}/modules/audio_coding/neteq/timestamp_scaler.cc
)

target_compile_options(libneteq PRIVATE
)

target_compile_definitions(libneteq PRIVATE
)

target_include_directories(libneteq PRIVATE

)
