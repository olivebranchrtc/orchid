set(rtcbase_SOURCES
		${webrtc_source_path}/rtc_base/async_invoker.cc
		${webrtc_source_path}/rtc_base/async_packet_socket.cc
		${webrtc_source_path}/rtc_base/async_resolver_interface.cc
		${webrtc_source_path}/rtc_base/async_socket.cc
		${webrtc_source_path}/rtc_base/async_tcp_socket.cc
		${webrtc_source_path}/rtc_base/async_udp_socket.cc
		${webrtc_source_path}/rtc_base/bit_buffer.cc
		${webrtc_source_path}/rtc_base/buffer_queue.cc
		${webrtc_source_path}/rtc_base/byte_buffer.cc
		${webrtc_source_path}/rtc_base/checks.cc
		${webrtc_source_path}/rtc_base/copy_on_write_buffer.cc
		${webrtc_source_path}/rtc_base/cpu_time.cc
		${webrtc_source_path}/rtc_base/crc32.cc
		${webrtc_source_path}/rtc_base/critical_section.cc
		${webrtc_source_path}/rtc_base/crypt_string.cc
		${webrtc_source_path}/rtc_base/data_rate_limiter.cc
		${webrtc_source_path}/rtc_base/event.cc
		${webrtc_source_path}/rtc_base/event_tracer.cc
		${webrtc_source_path}/rtc_base/experiments/alr_experiment.cc
		${webrtc_source_path}/rtc_base/experiments/balanced_degradation_settings.cc
		${webrtc_source_path}/rtc_base/experiments/cpu_speed_experiment.cc
		${webrtc_source_path}/rtc_base/experiments/experimental_screenshare_settings.cc
		${webrtc_source_path}/rtc_base/experiments/field_trial_list.cc
		${webrtc_source_path}/rtc_base/experiments/field_trial_parser.cc
		${webrtc_source_path}/rtc_base/experiments/field_trial_units.cc
		${webrtc_source_path}/rtc_base/experiments/jitter_upper_bound_experiment.cc
		${webrtc_source_path}/rtc_base/experiments/keyframe_interval_settings.cc
		${webrtc_source_path}/rtc_base/experiments/normalize_simulcast_size_experiment.cc
		${webrtc_source_path}/rtc_base/experiments/quality_scaler_settings.cc
		${webrtc_source_path}/rtc_base/experiments/quality_scaling_experiment.cc
		${webrtc_source_path}/rtc_base/experiments/rate_control_settings.cc
		${webrtc_source_path}/rtc_base/experiments/rtt_mult_experiment.cc
		${webrtc_source_path}/rtc_base/experiments/stable_target_rate_experiment.cc
		${webrtc_source_path}/rtc_base/experiments/struct_parameters_parser.cc
		${webrtc_source_path}/rtc_base/experiments/min_video_bitrate_experiment.cc
		${webrtc_source_path}/rtc_base/fake_clock.cc
		#${webrtc_source_path}/rtc_base/fake_ssl_identity.cc
		${webrtc_source_path}/rtc_base/file_rotating_stream.cc
		#${webrtc_source_path}/rtc_base/firewall_socket_server.cc
		${webrtc_source_path}/rtc_base/helpers.cc
		${webrtc_source_path}/rtc_base/http_common.cc
		${webrtc_source_path}/rtc_base/ifaddrs_android.cc
		${webrtc_source_path}/rtc_base/ifaddrs_converter.cc
		${webrtc_source_path}/rtc_base/ip_address.cc
		${webrtc_source_path}/rtc_base/location.cc
		${webrtc_source_path}/rtc_base/log_sinks.cc
		${webrtc_source_path}/rtc_base/logging.cc
		${webrtc_source_path}/rtc_base/memory/aligned_malloc.cc
		${webrtc_source_path}/rtc_base/memory/fifo_buffer.cc
		${webrtc_source_path}/rtc_base/memory_stream.cc
		${webrtc_source_path}/rtc_base/memory_usage.cc
		${webrtc_source_path}/rtc_base/message_digest.cc
		${webrtc_source_path}/rtc_base/message_handler.cc
		${webrtc_source_path}/rtc_base/message_queue.cc
		${webrtc_source_path}/rtc_base/net_helper.cc
		${webrtc_source_path}/rtc_base/net_helpers.cc
		${webrtc_source_path}/rtc_base/network.cc
		${webrtc_source_path}/rtc_base/network_monitor.cc
		${webrtc_source_path}/rtc_base/network/sent_packet.cc
		${webrtc_source_path}/rtc_base/null_socket_server.cc
		${webrtc_source_path}/rtc_base/numerics/exp_filter.cc
		${webrtc_source_path}/rtc_base/numerics/histogram_percentile_counter.cc
		${webrtc_source_path}/rtc_base/numerics/moving_average.cc
		${webrtc_source_path}/rtc_base/numerics/sample_counter.cc
		${webrtc_source_path}/rtc_base/numerics/samples_stats_counter.cc
		${webrtc_source_path}/rtc_base/openssl_adapter.cc
		${webrtc_source_path}/rtc_base/openssl_certificate.cc
		${webrtc_source_path}/rtc_base/openssl_digest.cc
		${webrtc_source_path}/rtc_base/openssl_identity.cc
		${webrtc_source_path}/rtc_base/openssl_session_cache.cc
		${webrtc_source_path}/rtc_base/openssl_stream_adapter.cc
		${webrtc_source_path}/rtc_base/openssl_utility.cc
		${webrtc_source_path}/rtc_base/physical_socket_server.cc
		${webrtc_source_path}/rtc_base/platform_thread.cc
		${webrtc_source_path}/rtc_base/platform_thread_types.cc
		${webrtc_source_path}/rtc_base/proxy_info.cc
		${webrtc_source_path}/rtc_base/proxy_server.cc
		${webrtc_source_path}/rtc_base/race_checker.cc
		${webrtc_source_path}/rtc_base/random.cc
		${webrtc_source_path}/rtc_base/rate_limiter.cc
		${webrtc_source_path}/rtc_base/rate_statistics.cc
		${webrtc_source_path}/rtc_base/rate_tracker.cc
		${webrtc_source_path}/rtc_base/rtc_certificate.cc
		${webrtc_source_path}/rtc_base/rtc_certificate_generator.cc
		${webrtc_source_path}/rtc_base/server_socket_adapters.cc
		${webrtc_source_path}/rtc_base/signal_thread.cc
		${webrtc_source_path}/rtc_base/socket.cc
		${webrtc_source_path}/rtc_base/socket_adapters.cc
		${webrtc_source_path}/rtc_base/socket_address.cc
		${webrtc_source_path}/rtc_base/socket_address_pair.cc
		${webrtc_source_path}/rtc_base/socket_stream.cc
		${webrtc_source_path}/rtc_base/ssl_adapter.cc
		${webrtc_source_path}/rtc_base/ssl_certificate.cc
		${webrtc_source_path}/rtc_base/ssl_fingerprint.cc
		${webrtc_source_path}/rtc_base/ssl_identity.cc
		${webrtc_source_path}/rtc_base/ssl_stream_adapter.cc
		${webrtc_source_path}/rtc_base/stream.cc
		${webrtc_source_path}/rtc_base/string_encode.cc
		${webrtc_source_path}/rtc_base/strings/audio_format_to_string.cc
		${webrtc_source_path}/rtc_base/strings/string_builder.cc
		${webrtc_source_path}/rtc_base/strings/json.cc
		${webrtc_source_path}/rtc_base/string_to_number.cc
		${webrtc_source_path}/rtc_base/string_utils.cc
		${webrtc_source_path}/rtc_base/synchronization/rw_lock_posix.cc
		${webrtc_source_path}/rtc_base/synchronization/rw_lock_wrapper.cc
		${webrtc_source_path}/rtc_base/synchronization/sequence_checker.cc
		${webrtc_source_path}/rtc_base/synchronization/yield_policy.cc
		${webrtc_source_path}/rtc_base/system/file_wrapper.cc
		${webrtc_source_path}/rtc_base/system/thread_registry.cc
		${webrtc_source_path}/rtc_base/system/warn_current_thread_is_deadlocked.cc
		${webrtc_source_path}/rtc_base/task_queue.cc
		${webrtc_source_path}/rtc_base/task_queue_stdlib.cc
		${webrtc_source_path}/rtc_base/task_utils/repeating_task.cc
		${webrtc_source_path}/rtc_base/third_party/base64/base64.cc
		${webrtc_source_path}/rtc_base/third_party/sigslot/sigslot.cc
		${webrtc_source_path}/rtc_base/thread.cc
		${webrtc_source_path}/rtc_base/time/timestamp_extrapolator.cc
		${webrtc_source_path}/rtc_base/timestamp_aligner.cc
		${webrtc_source_path}/rtc_base/time_utils.cc
		${webrtc_source_path}/rtc_base/unique_id_generator.cc
		${webrtc_source_path}/rtc_base/weak_ptr.cc
		${webrtc_source_path}/rtc_base/zero_memory.cc
		)

foreach(fileItem ${rtcbase_SOURCES})
	# Get the directory of the source file
	get_filename_component(PARENT_DIR "${fileItem}" DIRECTORY)
	# Remove common directory prefix to make the group
	string(REPLACE "${CMAKE_CURRENT_SOURCE_DIR}" "" GROUP "${PARENT_DIR}")
	# Make sure we are using windows slashes
	string(REPLACE "/" "\\" GROUP "${GROUP}")
	# Group into "Source Files" and "Header Files"
	set(GROUP "${GROUP}")
	source_group("${GROUP}" FILES "${fileItem}")
endforeach()

add_library(librtcbase OBJECT
		${rtcbase_SOURCES}
)

target_compile_options(librtcbase PRIVATE
)

target_compile_definitions(librtcbase PRIVATE
)

target_include_directories(librtcbase PRIVATE

)
