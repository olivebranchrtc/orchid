
add_library(libyuv OBJECT
		${libyuv_source_path}/source/compare.cc
		${libyuv_source_path}/source/compare_common.cc
		${libyuv_source_path}/source/compare_gcc.cc
		${libyuv_source_path}/source/convert.cc
		${libyuv_source_path}/source/convert_argb.cc
		${libyuv_source_path}/source/convert_from.cc
		${libyuv_source_path}/source/convert_from_argb.cc
		${libyuv_source_path}/source/convert_jpeg.cc
		${libyuv_source_path}/source/convert_to_argb.cc
		${libyuv_source_path}/source/convert_to_i420.cc
		${libyuv_source_path}/source/cpu_id.cc
		${libyuv_source_path}/source/mjpeg_decoder.cc
		${libyuv_source_path}/source/mjpeg_validate.cc
		${libyuv_source_path}/source/planar_functions.cc
		${libyuv_source_path}/source/compare_neon.cc
		${libyuv_source_path}/source/compare_neon64.cc
		${libyuv_source_path}/source/rotate.cc
		${libyuv_source_path}/source/rotate_any.cc
		${libyuv_source_path}/source/rotate_argb.cc
		${libyuv_source_path}/source/rotate_common.cc
		${libyuv_source_path}/source/rotate_gcc.cc
		${libyuv_source_path}/source/rotate_neon.cc
		${libyuv_source_path}/source/rotate_neon64.cc
		${libyuv_source_path}/source/row_any.cc
		${libyuv_source_path}/source/row_common.cc
		${libyuv_source_path}/source/row_gcc.cc
		${libyuv_source_path}/source/row_neon.cc
		${libyuv_source_path}/source/row_neon64.cc
		${libyuv_source_path}/source/scale.cc
		${libyuv_source_path}/source/scale_any.cc
		${libyuv_source_path}/source/scale_argb.cc
		${libyuv_source_path}/source/scale_common.cc
		${libyuv_source_path}/source/scale_gcc.cc
		${libyuv_source_path}/source/scale_neon.cc
		${libyuv_source_path}/source/scale_neon64.cc
		${libyuv_source_path}/source/video_common.cc
		${libyuv_source_path}/source/scale_uv.cc
)

target_compile_options(libyuv PRIVATE
)

target_compile_definitions(libyuv PRIVATE
)

target_include_directories(libyuv PRIVATE

)
