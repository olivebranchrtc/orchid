# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

set(CMAKE_CXX_STANDARD 14)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

set(CMAKE_OSX_SYSROOT "/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS15.5.sdk")
set(CMAKE_INSTALL_NAME_TOOL "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/install_name_tool")
set(CMAKE_TOOLCHAIN_FILE "${CMAKE_CURRENT_SOURCE_DIR}/ios.cmake")

set(IOS_PLATFORM "OS")
cmake_minimum_required(VERSION 3.18.1)

# Declares and names the project.

project("rtc_test")

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

# Searches for a specified prebuilt library and stores the path as a
# variable. Because CMake includes system libraries in the search path by
# default, you only need to specify the name of the public NDK library
# you want to add. CMake verifies that the library exists before
# completing its build.

# Specifies libraries CMake should link to your target library. You
# can link multiple libraries, such as libraries you define in this
# build script, prebuilt third-party libraries, or system libraries.

set (CURRENT_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set (SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../src)

include_directories(${SOURCE_DIR}/third_party/ffmpeg/ios/include)
include_directories(${SOURCE_DIR}/third_party/x264/binary/ios/include)
include_directories(${SOURCE_DIR}/third_party/libvpx/ios/include)
include_directories(${SOURCE_DIR}/third_party/opus/ios/include)
include_directories(${SOURCE_DIR}/third_party/opus/ios/include/opus)

set (FFMPEG_LIB "${SOURCE_DIR}/third_party/ffmpeg/ios/lib")
set (X264_LIB "${SOURCE_DIR}/third_party/x264/binary/ios/lib")
set (VPX_LIB "${SOURCE_DIR}/third_party/libvpx/ios/lib")
set (OPUS_LIB "${SOURCE_DIR}/third_party/opus/ios/lib")

include(${CMAKE_CURRENT_SOURCE_DIR}/libyipo_base.cmake)

SET_XCODE_PROPERTY("yipo_base" CODE_SIGN_IDENTITY "iPhone Developer")
SET_XCODE_PROPERTY("yipo_base" DEVELOPMENT_TEAM "G9BYFA8DX4")
SET_XCODE_PROPERTY("yipo_base" IPHONEOS_DEPLOYMENT_TARGET "15.5")
SET_XCODE_PROPERTY("yipo_base" SDKROOT "iphoneos")
#SET_XCODE_PROPERTY("yipo_base" ARCHS "arm64,armv7")
