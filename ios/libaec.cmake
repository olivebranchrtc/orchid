
add_library(libaec OBJECT
		${webrtc_source_path}/modules/audio_processing/aec/aec_core_neon.cc
		${webrtc_source_path}/modules/audio_processing/aec/aec_core.cc
		${webrtc_source_path}/modules/audio_processing/aec/aec_resampler.cc
		${webrtc_source_path}/modules/audio_processing/aec/echo_cancellation.cc
		${webrtc_source_path}/modules/audio_processing/aec_dump/null_aec_dump_factory.cc
)

target_compile_options(libaec PRIVATE
)

target_compile_definitions(libaec PRIVATE
)

target_include_directories(libaec PRIVATE

)
