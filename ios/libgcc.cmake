
add_library(libgcc OBJECT
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/acknowledged_bitrate_estimator.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/alr_detector.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/bitrate_estimator.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/congestion_window_pushback_controller.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/delay_based_bwe.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/link_capacity_estimator.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/loss_based_bandwidth_estimation.cc
		#${webrtc_source_path}/modules/congestion_controller/send_side_bwe/median_slope_estimator.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/probe_bitrate_estimator.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/probe_controller.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/send_side_bandwidth_estimation.cc
		${webrtc_source_path}/modules/congestion_controller/send_side_bwe/trendline_estimator.cc
		${webrtc_source_path}/modules/congestion_controller/rtp/control_handler.cc
		${webrtc_source_path}/modules/congestion_controller/rtp/transport_feedback_adapter.cc
		${webrtc_source_path}/modules/congestion_controller/goog_cc_network_control.cc
		${webrtc_source_path}/modules/congestion_controller/receive_side_congestion_controller.cc
)

target_compile_options(libgcc PRIVATE
)

target_compile_definitions(libgcc PRIVATE
)

target_include_directories(libgcc PRIVATE

)
