
add_library(libpacing OBJECT
		${webrtc_source_path}/modules/pacing/bitrate_prober.cc
		${webrtc_source_path}/modules/pacing/interval_budget.cc
		${webrtc_source_path}/modules/pacing/paced_sender.cc
		${webrtc_source_path}/modules/pacing/pacing_controller.cc
		${webrtc_source_path}/modules/pacing/packet_router.cc
		${webrtc_source_path}/modules/pacing/round_robin_packet_queue.cc
)

target_compile_options(libpacing PRIVATE
)

target_compile_definitions(libpacing PRIVATE
)

target_include_directories(libpacing PRIVATE

)
