
add_library(librbe OBJECT
		${webrtc_source_path}/modules/congestion_controller/receive_side_bwe/aimd_rate_control.cc
		${webrtc_source_path}/modules/congestion_controller/receive_side_bwe/bwe_defines.cc
		${webrtc_source_path}/modules/congestion_controller/receive_side_bwe/inter_arrival.cc
		${webrtc_source_path}/modules/congestion_controller/receive_side_bwe/overuse_detector.cc
		${webrtc_source_path}/modules/congestion_controller/receive_side_bwe/overuse_estimator.cc
		${webrtc_source_path}/modules/congestion_controller/receive_side_bwe/remote_bitrate_estimator_abs_send_time.cc
		${webrtc_source_path}/modules/congestion_controller/receive_side_bwe/remote_bitrate_estimator_single_stream.cc
		${webrtc_source_path}/modules/congestion_controller/receive_side_bwe/remote_estimator_proxy.cc
)

target_compile_options(librbe PRIVATE
)

target_compile_definitions(librbe PRIVATE
)

target_include_directories(librbe PRIVATE

)
