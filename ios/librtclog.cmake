
add_library(librtclog OBJECT
		${webrtc_source_path}/api/rtc_event_log_output_file.cc
		${webrtc_source_path}/api/rtc_event_log/rtc_event.cc
		${webrtc_source_path}/api/rtc_event_log/rtc_event_log.cc
		${webrtc_source_path}/api/rtc_event_log/rtc_event_log_factory.cc
		#${webrtc_source_path}/logging/rtc_event_log/encoder/rtc_event_log_encoder_legacy.cc
		#${webrtc_source_path}/logging/rtc_event_log/encoder/blob_encoding.cc
		#${webrtc_source_path}/logging/rtc_event_log/encoder/delta_encoding.cc
		#${webrtc_source_path}/logging/rtc_event_log/encoder/rtc_event_log_encoder_common.cc
		#${webrtc_source_path}/logging/rtc_event_log/encoder/rtc_event_log_encoder_new_format.cc
		#${webrtc_source_path}/logging/rtc_event_log/encoder/var_int.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_alr_state.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_audio_network_adaptation.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_audio_playout.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_audio_receive_stream_config.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_audio_send_stream_config.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_bwe_update_delay_based.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_bwe_update_loss_based.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_generic_ack_received.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_generic_packet_received.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_generic_packet_sent.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_probe_cluster_created.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_probe_result_failure.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_probe_result_success.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_route_change.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_rtcp_packet_incoming.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_rtcp_packet_outgoing.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_rtp_packet_incoming.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_rtp_packet_outgoing.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_video_receive_stream_config.cc
		${webrtc_source_path}/logging/rtc_event_log/events/rtc_event_video_send_stream_config.cc
		${webrtc_source_path}/logging/rtc_event_log/logged_events.cc
		#${webrtc_source_path}/logging/rtc_event_log/rtc_event_log_impl.cc
		${webrtc_source_path}/logging/rtc_event_log/rtc_event_processor.cc
		${webrtc_source_path}/logging/rtc_event_log/rtc_stream_config.cc
)

target_compile_options(librtclog PRIVATE
)

target_compile_definitions(librtclog PRIVATE
)

target_include_directories(librtclog PRIVATE

)
