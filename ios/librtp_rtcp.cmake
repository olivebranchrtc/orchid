
add_library(librtp_rtcp OBJECT
		${webrtc_source_path}/modules/rtp_rtcp/include/report_block_data.cc
		${webrtc_source_path}/modules/rtp_rtcp/include/rtp_rtcp_defines.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/fec_private_tables_bursty.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/fec_private_tables_random.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/fec_test_helper.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/flexfec_header_reader_writer.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/flexfec_receiver.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/flexfec_sender.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/forward_error_correction.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/forward_error_correction_internal.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/packet_loss_stats.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/playout_delay_oracle.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/receive_statistics_impl.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/remote_ntp_time_estimator.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_nack_stats.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/app.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/bye.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/common_header.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/compound_packet.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/dlrr.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/extended_jitter_report.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/extended_reports.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/fir.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/loss_notification.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/nack.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/pli.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/psfb.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/rapid_resync_request.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/receiver_report.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/remb.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/remote_estimate.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/report_block.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/rrtr.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/rtpfb.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/sdes.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/sender_report.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/target_bitrate.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/tmmb_item.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/tmmbn.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/tmmbr.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_packet/transport_feedback.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_receiver.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_sender.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_transceiver.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_transceiver_config.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtcp_transceiver_impl.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_dependency_descriptor_extension.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_dependency_descriptor_writer.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_dependency_descriptor_reader.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_format.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_format_h264.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_format_video_generic.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_format_vp8.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_format_vp9.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_generic_frame_descriptor.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_generic_frame_descriptor_extension.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_header_extension_map.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_header_extensions.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_header_extension_size.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_packet.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_packet_history.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_packet_received.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_packet_to_send.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_rtcp_impl.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_sender_audio.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_sender.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_sender_video.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_sequence_number_map.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_utility.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/rtp_video_header.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/source_tracker.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/time_util.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/tmmbr_help.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/ulpfec_generator.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/ulpfec_header_reader_writer.cc
		${webrtc_source_path}/modules/rtp_rtcp/source/ulpfec_receiver_impl.cc
)

target_compile_options(librtp_rtcp PRIVATE
)

target_compile_definitions(librtp_rtcp PRIVATE
)

target_include_directories(librtp_rtcp PRIVATE

)
