set (boringssl_source_path "${SOURCE_DIR}/include/boringssl")
set (rnnoise_source_path "${SOURCE_DIR}/third_party/rnnoise/src")
set (pffft_source_path "${SOURCE_DIR}/third_party/pffft/src")
set (libyuv_source_path "${SOURCE_DIR}/third_party/libyuv")

set (abseil_source_path "${SOURCE_DIR}/source/abseil-cpp")
set (jsoncpp_source_path "${SOURCE_DIR}/source/jsoncpp")
set (webrtc_source_path "${SOURCE_DIR}/source/webrtc_m79")

include(${CURRENT_DIR}/libboringssl.cmake)
include(${CURRENT_DIR}/libyuv.cmake)
include(${CURRENT_DIR}/librtcbase.cmake)
include(${CURRENT_DIR}/librtp_rtcp.cmake)
include(${CURRENT_DIR}/libaec.cmake)
include(${CURRENT_DIR}/libaecm.cmake)
include(${CURRENT_DIR}/libaec3.cmake)
include(${CURRENT_DIR}/libagc.cmake)
include(${CURRENT_DIR}/libagc2.cmake)
include(${CURRENT_DIR}/libvad.cmake)
include(${CURRENT_DIR}/libneteq.cmake)
include(${CURRENT_DIR}/libana.cmake)
include(${CURRENT_DIR}/libans.cmake)
include(${CURRENT_DIR}/libasp.cmake)
include(${CURRENT_DIR}/librtclog.cmake)
include(${CURRENT_DIR}/libgcc.cmake)
include(${CURRENT_DIR}/librbe.cmake)
include(${CURRENT_DIR}/libpacing.cmake)
include(${CURRENT_DIR}/libyipo_rtc.cmake)

include_directories(${SOURCE_DIR}/source/jsoncpp/include)
include_directories(${SOURCE_DIR}/third_party/libyuv/include)
include_directories(${SOURCE_DIR}/include/boringssl/src/include)
include_directories(${SOURCE_DIR}/third_party)

include_directories(${abseil_source_path})
include_directories(${boringssl_source_path}/src/include)
include_directories(${jsoncpp_source_path}/include)
include_directories(${jsoncpp_source_path}/src/lib_json)
include_directories(${libyuv_source_path}/include)
include_directories(${rnnoise_source_path})
include_directories(${webrtc_source_path})
include_directories(${webrtc_source_path}/common_audio/resampler/include)
include_directories(${webrtc_source_path}/common_audio/signal_processing/include)
include_directories(${webrtc_source_path}/common_audio/vad/include)
include_directories(${webrtc_source_path}/modules/audio_coding/codecs/isac/main/include)
include_directories(${SOURCE_DIR}/)
include_directories(${SOURCE_DIR}/third_party/ffmpeg)
include_directories(${webrtc_source_path}/modules/video_coding/codecs/ffmpeg_encoder/h264_parser)
include_directories(${webrtc_source_path}/sdk/objc)
include_directories(${webrtc_source_path}/sdk/objc/base)
include_directories(${webrtc_source_path}/sdk/objc/Framework/Headers/WebRTC)

message(STAGUS, "cmake dir: ${CMAKE_CURRENT_SOURCE_DIR}")

ADD_DEFINITIONS(
		-DOPENSSL_NO_ASM
        #-DENABLE_RTC_EVENT_LOG
		-DDISABLE_H265
		-DDYNAMIC_ANNOTATIONS_ENABLED=1
		-DEXPAT_RELATIVE_PATH
		-DHAVE_LRINT
		-DHAVE_LRINTF
		-DHAVE_NETINET_IN_H
		-DHAVE_WEBRTC_VIDEO
		-DHAVE_WEBRTC_VOICE
		-DJSON_USE_EXCEPTION=0
		-DNON_WINDOWS_DEFINE
		-DOPUS_BUILD
		-DOPUS_EXPORT=
		-DVAR_ARRAYS
		-DWEBRTC_APM_DEBUG_DUMP=0
		-DWEBRTC_CODEC_OPUS
		-DWEBRTC_CODEC_RED
		-DWEBRTC_INCLUDE_INTERNAL_AUDIO_DEVICE
		-DWEBRTC_INTELLIGIBILITY_ENHANCER=0
		-DWEBRTC_POSIX
		#-DWEBRTC_LINUX
		#-DWEBRTC_ANDROID
		-DWEBRTC_IOS
		#-DWEBRTC_MAC
		-DWEBRTC_NS_FLOAT
		-DWEBRTC_OPUS_SUPPORT_120MS_PTIME=0
		-DWEBRTC_OPUS_VARIABLE_COMPLEXITY=0
		-DWEBRTC_USE_BUILTIN_OPUS=1
		-DWTF_USE_DYNAMIC_ANNOTATIONS=1
		-DWEBRTC_USE_H264
		-DWEBRTC_USE_VP8
		-DRTC_SUPPORTS_METAL
)


if (WTF_CPU_ARM)
	ADD_DEFINITIONS(-DWEBRTC_ARCH_ARM=1)
elseif (WTF_CPU_ARM64)
	ADD_DEFINITIONS(-DWEBRTC_ARCH_ARM64=1)
endif()

set(webrtc_SOURCES
	${abseil_source_path}/absl/base/dynamic_annotations.cc
	${abseil_source_path}/absl/base/internal/raw_logging.cc
	${abseil_source_path}/absl/base/internal/throw_delegate.cc
	${abseil_source_path}/absl/strings/ascii.cc
	${abseil_source_path}/absl/strings/internal/memutil.cc
	${abseil_source_path}/absl/strings/match.cc
	${abseil_source_path}/absl/strings/string_view.cc
	${abseil_source_path}/absl/types/bad_optional_access.cc
	${abseil_source_path}/absl/types/bad_variant_access.cc
	${jsoncpp_source_path}/src/lib_json/json_reader.cpp
	${jsoncpp_source_path}/src/lib_json/json_value.cpp
	${jsoncpp_source_path}/src/lib_json/json_writer.cpp
	${pffft_source_path}/pffft.c
	${webrtc_source_path}/api/audio/audio_frame.cc
	${webrtc_source_path}/api/audio/channel_layout.cc
	${webrtc_source_path}/api/audio/echo_canceller3_config.cc
	${webrtc_source_path}/api/audio/echo_canceller3_factory.cc
	${webrtc_source_path}/api/audio_codecs/audio_codec_pair_id.cc
	${webrtc_source_path}/api/audio_codecs/audio_decoder.cc
	${webrtc_source_path}/api/audio_codecs/audio_encoder.cc
	${webrtc_source_path}/api/audio_codecs/audio_format.cc
	${webrtc_source_path}/api/audio_codecs/builtin_audio_decoder_factory.cc
	${webrtc_source_path}/api/audio_codecs/builtin_audio_encoder_factory.cc
	${webrtc_source_path}/api/audio_codecs/opus/audio_decoder_multi_channel_opus.cc
	${webrtc_source_path}/api/audio_codecs/opus/audio_decoder_opus.cc
	${webrtc_source_path}/api/audio_codecs/opus/audio_encoder_multi_channel_opus.cc
	${webrtc_source_path}/api/audio_codecs/opus/audio_encoder_multi_channel_opus_config.cc
	${webrtc_source_path}/api/audio_codecs/opus/audio_encoder_opus.cc
	${webrtc_source_path}/api/audio_codecs/opus/audio_encoder_opus_config.cc
	${webrtc_source_path}/api/audio_options.cc
	${webrtc_source_path}/api/call/transport.cc
	${webrtc_source_path}/api/crypto/crypto_options.cc
	${webrtc_source_path}/api/media_stream_interface.cc
	${webrtc_source_path}/api/media_types.cc
	${webrtc_source_path}/api/rtc_error.cc
	${webrtc_source_path}/api/rtp_headers.cc
	${webrtc_source_path}/api/rtp_packet_info.cc
	${webrtc_source_path}/api/rtp_parameters.cc
	${webrtc_source_path}/api/task_queue/default_task_queue_factory_stdlib.cc
	${webrtc_source_path}/api/task_queue/task_queue_base.cc
	${webrtc_source_path}/api/transport/bitrate_settings.cc
	${webrtc_source_path}/api/transport/field_trial_based_config.cc
	${webrtc_source_path}/api/transport/goog_cc_factory.cc
	${webrtc_source_path}/api/transport/media/audio_transport.cc
	${webrtc_source_path}/api/transport/media/video_transport.cc
	${webrtc_source_path}/api/transport/media/media_transport_config.cc
	${webrtc_source_path}/api/transport/media/media_transport_interface.cc
	${webrtc_source_path}/api/transport/network_types.cc
	${webrtc_source_path}/api/units/data_rate.cc
	${webrtc_source_path}/api/units/data_size.cc
	${webrtc_source_path}/api/units/time_delta.cc
	${webrtc_source_path}/api/units/timestamp.cc
	${webrtc_source_path}/api/video/builtin_video_bitrate_allocator_factory.cc
	${webrtc_source_path}/api/video/color_space.cc
	${webrtc_source_path}/api/video/encoded_frame.cc
	${webrtc_source_path}/api/video/encoded_image.cc
	${webrtc_source_path}/api/video/hdr_metadata.cc
	${webrtc_source_path}/api/video/i010_buffer.cc
	${webrtc_source_path}/api/video/i420_buffer.cc
	${webrtc_source_path}/api/video/video_bitrate_allocation.cc
	${webrtc_source_path}/api/video/video_bitrate_allocator.cc
	${webrtc_source_path}/api/video/video_content_type.cc
	${webrtc_source_path}/api/video/video_frame.cc
	${webrtc_source_path}/api/video/video_frame_buffer.cc
	${webrtc_source_path}/api/video/video_source_interface.cc
	${webrtc_source_path}/api/video/video_stream_encoder_create.cc
	${webrtc_source_path}/api/video/video_timing.cc
	${webrtc_source_path}/api/video/video_stream_encoder_observer.cc
	${webrtc_source_path}/api/video_codecs/builtin_video_decoder_factory.cc
	${webrtc_source_path}/api/video_codecs/builtin_video_encoder_factory.cc
	${webrtc_source_path}/api/video_codecs/sdp_video_format.cc
	${webrtc_source_path}/api/video_codecs/video_codec.cc
	${webrtc_source_path}/api/video_codecs/video_decoder.cc
	${webrtc_source_path}/api/video_codecs/video_decoder_factory.cc
	${webrtc_source_path}/api/video_codecs/video_decoder_software_fallback_wrapper.cc
	${webrtc_source_path}/api/video_codecs/video_encoder.cc
	${webrtc_source_path}/api/video_codecs/video_encoder_config.cc
	${webrtc_source_path}/api/video_codecs/video_encoder_software_fallback_wrapper.cc
	${webrtc_source_path}/api/video_codecs/vp8_frame_config.cc
	${webrtc_source_path}/api/video_codecs/vp8_temporal_layers.cc
	${webrtc_source_path}/api/video_codecs/vp8_temporal_layers_factory.cc
	${webrtc_source_path}/audio/audio_level.cc
	${webrtc_source_path}/audio/audio_receive_stream.cc
	${webrtc_source_path}/audio/audio_send_stream.cc
	${webrtc_source_path}/audio/audio_state.cc
	${webrtc_source_path}/audio/audio_transport_impl.cc
	${webrtc_source_path}/audio/channel_receive.cc
	${webrtc_source_path}/audio/channel_send.cc
	${webrtc_source_path}/audio/null_audio_poller.cc
	${webrtc_source_path}/audio/remix_resample.cc
	${webrtc_source_path}/audio/utility/audio_frame_operations.cc
	${webrtc_source_path}/audio/utility/channel_mixer.cc
	${webrtc_source_path}/audio/utility/channel_mixing_matrix.cc
	${webrtc_source_path}/audio/transport_feedback_packet_loss_tracker.cc
	${webrtc_source_path}/call/audio_receive_stream.cc
	${webrtc_source_path}/call/audio_send_stream.cc
	${webrtc_source_path}/call/audio_state.cc
	${webrtc_source_path}/call/bitrate_allocator.cc
	${webrtc_source_path}/call/call.cc
	${webrtc_source_path}/call/call_config.cc
	${webrtc_source_path}/call/call_factory.cc
	${webrtc_source_path}/call/degraded_call.cc
	${webrtc_source_path}/call/fake_network_pipe.cc
	${webrtc_source_path}/call/flexfec_receive_stream.cc
	${webrtc_source_path}/call/flexfec_receive_stream_impl.cc
	${webrtc_source_path}/call/receive_time_calculator.cc
	${webrtc_source_path}/call/rtcp_demuxer.cc
	${webrtc_source_path}/call/rtp_bitrate_configurator.cc
	${webrtc_source_path}/call/rtp_config.cc
	${webrtc_source_path}/call/rtp_demuxer.cc
	${webrtc_source_path}/call/rtp_payload_params.cc
	${webrtc_source_path}/call/rtp_rtcp_demuxer_helper.cc
	${webrtc_source_path}/call/rtp_stream_receiver_controller.cc
	${webrtc_source_path}/call/rtp_transport_controller_send.cc
	${webrtc_source_path}/call/rtp_video_sender.cc
	${webrtc_source_path}/call/rtx_receive_stream.cc
	${webrtc_source_path}/call/simulated_network.cc
	${webrtc_source_path}/call/syncable.cc
	${webrtc_source_path}/call/video_receive_stream.cc
	${webrtc_source_path}/call/video_send_stream.cc
	${webrtc_source_path}/common_audio/audio_converter.cc
	${webrtc_source_path}/common_audio/audio_util.cc
	${webrtc_source_path}/common_audio/channel_buffer.cc
	${webrtc_source_path}/common_audio/fir_filter_c.cc
	${webrtc_source_path}/common_audio/fir_filter_factory.cc
	#${webrtc_source_path}/common_audio/real_fourier.cc
	#${webrtc_source_path}/common_audio/real_fourier_ooura.cc
	${webrtc_source_path}/common_audio/resampler/push_resampler.cc
	${webrtc_source_path}/common_audio/resampler/push_sinc_resampler.cc
	${webrtc_source_path}/common_audio/resampler/resampler.cc
	${webrtc_source_path}/common_audio/resampler/sinc_resampler.cc
	${webrtc_source_path}/common_audio/ring_buffer.c
	${webrtc_source_path}/common_audio/smoothing_filter.cc
	${webrtc_source_path}/common_audio/sparse_fir_filter.cc
	${webrtc_source_path}/common_audio/smoothing_filter.cc
	${webrtc_source_path}/common_audio/third_party/fft4g/fft4g.c
	${webrtc_source_path}/common_audio/third_party/spl_sqrt_floor/spl_sqrt_floor.c
	${webrtc_source_path}/common_audio/wav_file.cc
	${webrtc_source_path}/common_audio/wav_header.cc
	${webrtc_source_path}/common_audio/window_generator.cc
	${webrtc_source_path}/common_video/bitrate_adjuster.cc
	${webrtc_source_path}/common_video/frame_rate_estimator.cc
	${webrtc_source_path}/common_video/generic_frame_descriptor/generic_frame_info.cc
	${webrtc_source_path}/common_video/h264/h264_bitstream_parser.cc
	${webrtc_source_path}/common_video/h264/h264_common.cc
	${webrtc_source_path}/common_video/h264/pps_parser.cc
	${webrtc_source_path}/common_video/h264/sps_parser.cc
	${webrtc_source_path}/common_video/h264/sps_vui_rewriter.cc
	${webrtc_source_path}/common_video/i420_buffer_pool.cc
	${webrtc_source_path}/common_video/incoming_video_stream.cc
	${webrtc_source_path}/common_video/libyuv/webrtc_libyuv.cc
	${webrtc_source_path}/common_video/video_frame_buffer.cc
	${webrtc_source_path}/common_video/video_render_frames.cc
	${webrtc_source_path}/media/base/adapted_video_track_source.cc
	${webrtc_source_path}/media/base/codec.cc
	${webrtc_source_path}/media/base/h264_profile_level_id.cc
	${webrtc_source_path}/media/base/media_constants.cc
	${webrtc_source_path}/media/base/video_adapter.cc
	${webrtc_source_path}/media/base/video_broadcaster.cc
	${webrtc_source_path}/media/base/video_common.cc
	${webrtc_source_path}/media/base/video_source_base.cc
	${webrtc_source_path}/media/base/vp9_profile.cc
	${webrtc_source_path}/media/engine/adm_helpers.cc
	${webrtc_source_path}/media/engine/apm_helpers.cc
	${webrtc_source_path}/media/engine/encoder_simulcast_proxy.cc
	${webrtc_source_path}/media/engine/internal_decoder_factory.cc
	${webrtc_source_path}/media/engine/internal_encoder_factory.cc
	${webrtc_source_path}/media/engine/simulcast.cc
	${webrtc_source_path}/media/engine/simulcast_encoder_adapter.cc
	${webrtc_source_path}/modules/audio_coding/acm2/acm_receiver.cc
	${webrtc_source_path}/modules/audio_coding/acm2/acm_resampler.cc
	${webrtc_source_path}/modules/audio_coding/acm2/audio_coding_module.cc
	${webrtc_source_path}/modules/audio_coding/acm2/call_statistics.cc
	${webrtc_source_path}/modules/audio_coding/codecs/cng/audio_encoder_cng.cc
	${webrtc_source_path}/modules/audio_coding/codecs/cng/webrtc_cng.cc
	${webrtc_source_path}/modules/audio_coding/codecs/isac/main/source/filter_functions.c
	${webrtc_source_path}/modules/audio_coding/codecs/isac/main/source/isac_vad.c
	${webrtc_source_path}/modules/audio_coding/codecs/isac/main/source/pitch_estimator.c
	${webrtc_source_path}/modules/audio_coding/codecs/isac/main/source/pitch_filter.c
	${webrtc_source_path}/modules/audio_coding/codecs/isac/main/source/pitch_gain_tables.c
	${webrtc_source_path}/modules/audio_coding/codecs/isac/main/source/pitch_lag_tables.c
	${webrtc_source_path}/modules/audio_coding/codecs/legacy_encoded_audio_frame.cc
	${webrtc_source_path}/modules/audio_coding/codecs/opus/opus_interface.c
	${webrtc_source_path}/modules/audio_coding/codecs/opus/audio_coder_opus_common.cc
	${webrtc_source_path}/modules/audio_coding/codecs/opus/audio_decoder_multi_channel_opus_impl.cc
	${webrtc_source_path}/modules/audio_coding/codecs/opus/audio_decoder_opus.cc
	${webrtc_source_path}/modules/audio_coding/codecs/opus/audio_encoder_multi_channel_opus_impl.cc
	${webrtc_source_path}/modules/audio_coding/codecs/opus/audio_encoder_opus.cc
	${webrtc_source_path}/modules/audio_coding/codecs/red/audio_encoder_copy_red.cc
	${webrtc_source_path}/modules/audio_device/audio_device_data_observer.cc
	${webrtc_source_path}/modules/audio_device/audio_device_buffer.cc
	${webrtc_source_path}/modules/audio_device/audio_device_generic.cc
	${webrtc_source_path}/modules/audio_device/audio_device_impl.cc
	${webrtc_source_path}/modules/audio_device/dummy/audio_device_dummy.cc
	${webrtc_source_path}/modules/audio_device/dummy/file_audio_device.cc
	${webrtc_source_path}/modules/audio_device/dummy/file_audio_device_factory.cc
	${webrtc_source_path}/modules/audio_device/fine_audio_buffer.cc
	#${webrtc_source_path}/modules/audio_device/android/audio_manager.cc
	#${webrtc_source_path}/modules/audio_device/android/audio_record_jni.cc
	#${webrtc_source_path}/modules/audio_device/android/audio_track_jni.cc
	#${webrtc_source_path}/modules/audio_device/android/build_info.cc
	#${webrtc_source_path}/modules/audio_device/android/ensure_initialized.cc
	#${webrtc_source_path}/modules/audio_device/android/opensles_common.cc
	#${webrtc_source_path}/modules/audio_device/android/opensles_player.cc
	#${webrtc_source_path}/modules/audio_device/android/opensles_recorder.cc
	${webrtc_source_path}/modules/audio_mixer/audio_frame_manipulator.cc
	${webrtc_source_path}/modules/audio_mixer/audio_mixer_impl.cc
	${webrtc_source_path}/modules/audio_mixer/default_output_rate_calculator.cc
	${webrtc_source_path}/modules/audio_mixer/frame_combiner.cc
	${webrtc_source_path}/modules/audio_processing/audio_buffer.cc
	${webrtc_source_path}/modules/audio_processing/audio_processing_impl.cc
	${webrtc_source_path}/modules/audio_processing/echo_control_mobile_impl.cc
	${webrtc_source_path}/modules/audio_processing/echo_cancellation_impl.cc
	${webrtc_source_path}/modules/audio_processing/echo_detector/circular_buffer.cc
	${webrtc_source_path}/modules/audio_processing/echo_detector/mean_variance_estimator.cc
	${webrtc_source_path}/modules/audio_processing/echo_detector/moving_max.cc
	${webrtc_source_path}/modules/audio_processing/echo_detector/normalized_covariance_estimator.cc
	${webrtc_source_path}/modules/audio_processing/gain_control_impl.cc
	${webrtc_source_path}/modules/audio_processing/gain_controller2.cc
	${webrtc_source_path}/modules/audio_processing/gain_control_for_experimental_agc.cc
	${webrtc_source_path}/modules/audio_processing/high_pass_filter.cc
	${webrtc_source_path}/modules/audio_processing/level_estimator.cc
	${webrtc_source_path}/modules/audio_processing/include/aec_dump.cc
	${webrtc_source_path}/modules/audio_processing/include/audio_processing.cc
	${webrtc_source_path}/modules/audio_processing/include/audio_processing_statistics.cc
	${webrtc_source_path}/modules/audio_processing/include/config.cc
	${webrtc_source_path}/modules/audio_processing/logging/apm_data_dumper.cc
	${webrtc_source_path}/modules/audio_processing/residual_echo_detector.cc
	${webrtc_source_path}/modules/audio_processing/rms_level.cc
	${webrtc_source_path}/modules/audio_processing/splitting_filter.cc
	${webrtc_source_path}/modules/audio_processing/three_band_filter_bank.cc
	${webrtc_source_path}/modules/audio_processing/transient/file_utils.cc
	${webrtc_source_path}/modules/audio_processing/transient/moving_moments.cc
	${webrtc_source_path}/modules/audio_processing/transient/transient_detector.cc
	${webrtc_source_path}/modules/audio_processing/transient/transient_suppressor.cc
	${webrtc_source_path}/modules/audio_processing/transient/wpd_node.cc
	${webrtc_source_path}/modules/audio_processing/transient/wpd_tree.cc
	${webrtc_source_path}/modules/audio_processing/typing_detection.cc
	${webrtc_source_path}/modules/audio_processing/voice_detection.cc
	${webrtc_source_path}/modules/audio_processing/utility/block_mean_calculator.cc
	${webrtc_source_path}/modules/audio_processing/utility/cascaded_biquad_filter.cc
	${webrtc_source_path}/modules/audio_processing/utility/delay_estimator.cc
	${webrtc_source_path}/modules/audio_processing/utility/delay_estimator_wrapper.cc
	${webrtc_source_path}/modules/audio_processing/utility/ooura_fft.cc
	${webrtc_source_path}/modules/audio_processing/utility/pffft_wrapper.cc
	${webrtc_source_path}/modules/include/module_common_types.cc
	${webrtc_source_path}/modules/third_party/fft/fft.c
	${webrtc_source_path}/modules/utility/source/process_thread_impl.cc
	#${webrtc_source_path}/modules/utility/source/jvm_android.cc
	#${webrtc_source_path}/modules/utility/source/helpers_android.cc
	${webrtc_source_path}/modules/video_coding/codec_timer.cc
	${webrtc_source_path}/modules/video_coding/decoder_database.cc
	${webrtc_source_path}/modules/video_coding/encoded_frame.cc
	${webrtc_source_path}/modules/video_coding/fec_controller_default.cc
	${webrtc_source_path}/modules/video_coding/frame_buffer2.cc
	${webrtc_source_path}/modules/video_coding/frame_object.cc
	${webrtc_source_path}/modules/video_coding/generic_decoder.cc
	${webrtc_source_path}/modules/video_coding/h264_sprop_parameter_sets.cc
	${webrtc_source_path}/modules/video_coding/h264_sps_pps_tracker.cc
	${webrtc_source_path}/modules/video_coding/histogram.cc
	${webrtc_source_path}/modules/video_coding/include/video_codec_interface.cc
	${webrtc_source_path}/modules/video_coding/inter_frame_delay.cc
	${webrtc_source_path}/modules/video_coding/jitter_estimator.cc
	${webrtc_source_path}/modules/video_coding/loss_notification_controller.cc
	${webrtc_source_path}/modules/video_coding/media_opt_util.cc
	${webrtc_source_path}/modules/video_coding/nack_module.cc
	${webrtc_source_path}/modules/video_coding/packet_buffer.cc
	${webrtc_source_path}/modules/video_coding/packet.cc
	${webrtc_source_path}/modules/video_coding/rtp_frame_reference_finder.cc
	${webrtc_source_path}/modules/video_coding/rtt_filter.cc
	${webrtc_source_path}/modules/video_coding/timestamp_map.cc
	${webrtc_source_path}/modules/video_coding/timing.cc
	${webrtc_source_path}/modules/video_coding/utility/decoded_frames_history.cc
	${webrtc_source_path}/modules/video_coding/utility/default_video_bitrate_allocator.cc
	${webrtc_source_path}/modules/video_coding/utility/frame_dropper.cc
	${webrtc_source_path}/modules/video_coding/utility/framerate_controller.cc
	${webrtc_source_path}/modules/video_coding/utility/ivf_file_writer.cc
	${webrtc_source_path}/modules/video_coding/utility/quality_scaler.cc
	${webrtc_source_path}/modules/video_coding/utility/simulcast_rate_allocator.cc
	${webrtc_source_path}/modules/video_coding/utility/simulcast_utility.cc
	${webrtc_source_path}/modules/video_coding/utility/vp8_header_parser.cc
	${webrtc_source_path}/modules/video_coding/utility/vp9_uncompressed_header_parser.cc
	${webrtc_source_path}/modules/video_coding/video_codec_initializer.cc
	${webrtc_source_path}/modules/video_coding/video_coding_defines.cc
	${webrtc_source_path}/modules/video_coding/video_receiver2.cc
	${webrtc_source_path}/modules/video_coding/codecs/ffmpeg_encoder/ff_video_encoder.cc
	${webrtc_source_path}/modules/video_coding/codecs/ffmpeg_encoder/h264_parser/h264_nal_parser.cc
	${webrtc_source_path}/modules/video_coding/codecs/h264/h264_color_space.cc
	${webrtc_source_path}/modules/video_coding/codecs/h264/h264_decoder_impl.cc
	${webrtc_source_path}/modules/video_coding/codecs/h264/h264_encoder_impl_ffmpeg.cc
	${webrtc_source_path}/modules/video_coding/codecs/h264/h264.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp8/default_temporal_layers.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp8/libvpx_interface.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp8/libvpx_vp8_decoder.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp8/libvpx_vp8_encoder.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp8/screenshare_layers.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp8/temporal_layers_checker.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp9/svc_config.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp9/svc_rate_allocator.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp9/vp9_frame_buffer_pool.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp9/vp9_impl.cc
	${webrtc_source_path}/modules/video_coding/codecs/vp9/vp9.cc
	${webrtc_source_path}/pc/video_track.cc
	${webrtc_source_path}/system_wrappers/source/clock.cc
	${webrtc_source_path}/system_wrappers/source/cpu_features.cc
	${webrtc_source_path}/system_wrappers/source/cpu_info.cc
	${webrtc_source_path}/system_wrappers/source/event.cc
	${webrtc_source_path}/system_wrappers/source/field_trial.cc
	${webrtc_source_path}/system_wrappers/source/metrics.cc
	${webrtc_source_path}/system_wrappers/source/rtp_to_ntp_estimator.cc
	${webrtc_source_path}/system_wrappers/source/sleep.cc
	${webrtc_source_path}/video/buffered_frame_decryptor.cc
	${webrtc_source_path}/video/call_stats.cc
	${webrtc_source_path}/video/encoder_bitrate_adjuster.cc
	${webrtc_source_path}/video/encoder_overshoot_detector.cc
	${webrtc_source_path}/video/encoder_rtcp_feedback.cc
	${webrtc_source_path}/video/frame_dumping_decoder.cc
	${webrtc_source_path}/video/frame_encode_metadata_writer.cc
	${webrtc_source_path}/video/overuse_frame_detector.cc
	${webrtc_source_path}/video/quality_limitation_reason_tracker.cc
	${webrtc_source_path}/video/quality_threshold.cc
	${webrtc_source_path}/video/receive_statistics_proxy.cc
	${webrtc_source_path}/video/report_block_stats.cc
	${webrtc_source_path}/video/rtp_streams_synchronizer.cc
	${webrtc_source_path}/video/rtp_video_stream_receiver.cc
	${webrtc_source_path}/video/send_delay_stats.cc
	${webrtc_source_path}/video/send_statistics_proxy.cc
	${webrtc_source_path}/video/stats_counter.cc
	${webrtc_source_path}/video/stream_synchronization.cc
	${webrtc_source_path}/video/transport_adapter.cc
	${webrtc_source_path}/video/video_quality_observer.cc
	${webrtc_source_path}/video/video_receive_stream.cc
	${webrtc_source_path}/video/video_send_stream.cc
	${webrtc_source_path}/video/video_send_stream_impl.cc
	${webrtc_source_path}/video/video_stream_decoder.cc
	${webrtc_source_path}/video/video_stream_encoder.cc
	$<TARGET_OBJECTS:libboringssl>
	$<TARGET_OBJECTS:libyuv>
	$<TARGET_OBJECTS:librtcbase>
	$<TARGET_OBJECTS:librtp_rtcp>
	$<TARGET_OBJECTS:libaec>
	$<TARGET_OBJECTS:libaecm>
	$<TARGET_OBJECTS:libaec3>
	$<TARGET_OBJECTS:libagc>
	$<TARGET_OBJECTS:libagc2>
	$<TARGET_OBJECTS:libvad>
	$<TARGET_OBJECTS:libneteq>
	$<TARGET_OBJECTS:libana>
	$<TARGET_OBJECTS:libans>
	$<TARGET_OBJECTS:libasp>
	$<TARGET_OBJECTS:librtclog>
	$<TARGET_OBJECTS:libgcc>
	$<TARGET_OBJECTS:librbe>
	$<TARGET_OBJECTS:libpacing>
	$<TARGET_OBJECTS:libyipo_rtc>
)

if (WTF_CPU_X86_64 OR WTF_CPU_X86)
	list(APPEND webrtc_SOURCES
		${webrtc_source_path}/common_audio/fir_filter_sse.cc
		${webrtc_source_path}/common_audio/resampler/sinc_resampler_sse.cc
		${webrtc_source_path}/modules/audio_processing/utility/ooura_fft_sse2.cc
		${webrtc_source_path}/modules/video_processing/util/denoiser_filter_sse2.cc
	)
endif()

foreach(fileItem ${webrtc_SOURCES})
	# Get the directory of the source file
	get_filename_component(PARENT_DIR "${fileItem}" DIRECTORY)
	# Remove common directory prefix to make the group
	string(REPLACE "${CMAKE_CURRENT_SOURCE_DIR}" "" GROUP "${PARENT_DIR}")
	# Make sure we are using windows slashes
	string(REPLACE "/" "\\" GROUP "${GROUP}")
	# Group into "Source Files" and "Header Files"
	set(GROUP "${GROUP}")
	source_group("${GROUP}" FILES "${fileItem}")
endforeach()

set(OBJC_SRCS
		${webrtc_source_path}/sdk/objc/api/peerconnection/RTCEncodedImage+Private.mm
		${webrtc_source_path}/sdk/objc/api/peerconnection/RTCRtpFragmentationHeader+Private.mm
		${webrtc_source_path}/sdk/objc/api/peerconnection/RTCVideoEncoderSettings+Private.mm
		${webrtc_source_path}/sdk/objc/api/peerconnection/RTCVideoCodecInfo+Private.mm
		${webrtc_source_path}/sdk/objc/api/RTCVideoRendererAdapter.mm
		${webrtc_source_path}/sdk/objc/api/logging/RTCCallbackLogger.mm
		${webrtc_source_path}/sdk/objc/api/video_codec/RTCVideoCodecConstants.mm
		${webrtc_source_path}/sdk/objc/api/video_codec/RTCVideoDecoderVP8.mm
		${webrtc_source_path}/sdk/objc/api/video_codec/RTCVideoDecoderVP9.mm
		${webrtc_source_path}/sdk/objc/api/video_codec/RTCVideoEncoderVP8.mm
		${webrtc_source_path}/sdk/objc/api/video_codec/RTCVideoEncoderVP9.mm
		${webrtc_source_path}/sdk/objc/api/video_codec/RTCWrappedNativeVideoDecoder.mm
		${webrtc_source_path}/sdk/objc/api/video_codec/RTCWrappedNativeVideoEncoder.mm
		${webrtc_source_path}/sdk/objc/api/video_frame_buffer/RTCNativeI420Buffer.mm
		${webrtc_source_path}/sdk/objc/api/video_frame_buffer/RTCNativeMutableI420Buffer.mm
		${webrtc_source_path}/sdk/media_constraints.cc
		${webrtc_source_path}/sdk/objc/base/RTCEncodedImage.m
		${webrtc_source_path}/sdk/objc/base/RTCLogging.mm
		${webrtc_source_path}/sdk/objc/base/RTCRtpFragmentationHeader.m
		${webrtc_source_path}/sdk/objc/base/RTCVideoCapturer.m
		${webrtc_source_path}/sdk/objc/base/RTCVideoCodecInfo.m
		${webrtc_source_path}/sdk/objc/base/RTCVideoEncoderQpThresholds.m
		${webrtc_source_path}/sdk/objc/base/RTCVideoEncoderSettings.m
		${webrtc_source_path}/sdk/objc/base/RTCVideoFrame.mm
		${webrtc_source_path}/sdk/objc/components/audio/RTCAudioSession.mm
		${webrtc_source_path}/sdk/objc/components/audio/RTCAudioSession+Configuration.mm
		${webrtc_source_path}/sdk/objc/components/audio/RTCAudioSessionConfiguration.m
		${webrtc_source_path}/sdk/objc/components/audio/RTCNativeAudioSessionDelegateAdapter.mm
		${webrtc_source_path}/sdk/objc/components/capturer/RTCCameraVideoCapturer.m
		${webrtc_source_path}/sdk/objc/components/capturer/RTCFileVideoCapturer.m
		${webrtc_source_path}/sdk/objc/components/renderer/opengl/RTCDefaultShader.mm
		${webrtc_source_path}/sdk/objc/components/renderer/opengl/RTCDisplayLinkTimer.m
		${webrtc_source_path}/sdk/objc/components/renderer/opengl/RTCEAGLVideoView.m
		${webrtc_source_path}/sdk/objc/components/renderer/opengl/RTCI420TextureCache.mm
		${webrtc_source_path}/sdk/objc/components/renderer/opengl/RTCNSGLVideoView.m
		${webrtc_source_path}/sdk/objc/components/renderer/opengl/RTCNV12TextureCache.m
		${webrtc_source_path}/sdk/objc/components/renderer/opengl/RTCShader.mm
		${webrtc_source_path}/sdk/objc/components/renderer/metal/RTCMTLI420Renderer.mm
		#${webrtc_source_path}/sdk/objc/components/renderer/metal/RTCMTLNSVideoView.m
		${webrtc_source_path}/sdk/objc/components/renderer/metal/RTCMTLNV12Renderer.mm
		${webrtc_source_path}/sdk/objc/components/renderer/metal/RTCMTLRenderer.mm
		${webrtc_source_path}/sdk/objc/components/renderer/metal/RTCMTLRGBRenderer.mm
		${webrtc_source_path}/sdk/objc/components/renderer/metal/RTCMTLVideoView.m
		${webrtc_source_path}/sdk/objc/components/video_codec/helpers.cc
		${webrtc_source_path}/sdk/objc/components/video_codec/nalu_rewriter.cc
		${webrtc_source_path}/sdk/objc/components/video_codec/RTCCodecSpecificInfoH264.mm
		${webrtc_source_path}/sdk/objc/components/video_codec/RTCDefaultVideoDecoderFactory.m
		${webrtc_source_path}/sdk/objc/components/video_codec/RTCDefaultVideoEncoderFactory.m
		${webrtc_source_path}/sdk/objc/components/video_codec/RTCH264ProfileLevelId.mm
		${webrtc_source_path}/sdk/objc/components/video_codec/RTCVideoDecoderFactoryH264.m
		${webrtc_source_path}/sdk/objc/components/video_codec/RTCVideoDecoderH264.mm
		${webrtc_source_path}/sdk/objc/components/video_codec/RTCVideoEncoderFactoryH264.m
		${webrtc_source_path}/sdk/objc/components/video_codec/RTCVideoEncoderH264.mm
		${webrtc_source_path}/sdk/objc/components/video_codec/UIDevice+H264Profile.mm
		${webrtc_source_path}/sdk/objc/components/video_frame_buffer/RTCCVPixelBuffer.mm
		${webrtc_source_path}/sdk/objc/helpers/AVCaptureSession+DevicePosition.mm
		${webrtc_source_path}/sdk/objc/helpers/noop.mm
		${webrtc_source_path}/sdk/objc/helpers/NSString+StdString.mm
		${webrtc_source_path}/sdk/objc/helpers/RTCCameraPreviewView.m
		${webrtc_source_path}/sdk/objc/helpers/RTCDispatcher.m
		${webrtc_source_path}/sdk/objc/helpers/UIDevice+RTCDevice.mm
		${webrtc_source_path}/sdk/objc/native/api/audio_device_module.mm
		${webrtc_source_path}/sdk/objc/native/api/video_capturer.mm
		${webrtc_source_path}/sdk/objc/native/api/video_decoder_factory.mm
		${webrtc_source_path}/sdk/objc/native/api/video_encoder_factory.mm
		${webrtc_source_path}/sdk/objc/native/api/video_frame.mm
		${webrtc_source_path}/sdk/objc/native/api/audio_device_module.mm
		${webrtc_source_path}/sdk/objc/native/api/video_renderer.mm
		${webrtc_source_path}/sdk/objc/native/src/audio/audio_device_ios.mm
		${webrtc_source_path}/sdk/objc/native/src/audio/audio_device_module_ios.mm
		${webrtc_source_path}/sdk/objc/native/src/audio/helpers.mm
		${webrtc_source_path}/sdk/objc/native/src/audio/voice_processing_audio_unit.mm
		${webrtc_source_path}/sdk/objc/native/src/objc_frame_buffer.mm
		${webrtc_source_path}/sdk/objc/native/src/objc_video_decoder_factory.mm
		${webrtc_source_path}/sdk/objc/native/src/objc_video_encoder_factory.mm
		${webrtc_source_path}/sdk/objc/native/src/objc_video_frame.mm
		${webrtc_source_path}/sdk/objc/native/src/objc_video_renderer.mm
		${webrtc_source_path}/sdk/objc/native/src/objc_video_track_source.mm
		)

foreach(fileItem ${OBJC_SRCS})
	# Get the directory of the source file
	get_filename_component(PARENT_DIR "${fileItem}" DIRECTORY)
	# Remove common directory prefix to make the group
	string(REPLACE "${CMAKE_CURRENT_SOURCE_DIR}" "" GROUP "${PARENT_DIR}")
	# Make sure we are using windows slashes
	string(REPLACE "/" "\\" GROUP "${GROUP}")
	# Group into "Source Files" and "Header Files"
	set(GROUP "${GROUP}")
	source_group("${GROUP}" FILES "${fileItem}")
endforeach()

add_library(yipo_base
		SHARED
		${webrtc_SOURCES}
		${OBJC_SRCS}
		)

find_library(x264-lib x264 PATHS ${X264_LIB} NO_CMAKE_FIND_ROOT_PATH)

find_library(avfilter-lib avfilter PATHS ${FFMPEG_LIB} NO_CMAKE_FIND_ROOT_PATH)
find_library(avcodec-lib avcodec PATHS ${FFMPEG_LIB}  NO_CMAKE_FIND_ROOT_PATH)
find_library(avformat-lib avformat PATHS ${FFMPEG_LIB}  NO_CMAKE_FIND_ROOT_PATH)
find_library(avutil-lib avutil PATHS ${FFMPEG_LIB}  NO_CMAKE_FIND_ROOT_PATH)
find_library(swresample-lib swresample PATHS ${FFMPEG_LIB}  NO_CMAKE_FIND_ROOT_PATH)
find_library(swscale-lib swscale PATHS ${FFMPEG_LIB}  NO_CMAKE_FIND_ROOT_PATH)
find_library(vpx-lib vpx PATHS ${VPX_LIB}  NO_CMAKE_FIND_ROOT_PATH)
find_library(opus-lib opus PATHS ${OPUS_LIB}  NO_CMAKE_FIND_ROOT_PATH)

target_link_libraries( # Specifies the target library.
		yipo_base
		${opus-lib}
		${avcodec-lib}
		${avutil-lib}
		${swresample-lib}
		${x264-lib}
		${vpx-lib}
		"-framework Foundation"
		"-framework VideoToolbox"
		"-framework AudioToolbox"
		"-framework CoreMedia"
		"-framework CoreVideo"
		"-framework CoreFoundation"
		"-framework AVFoundation"
		"-framework OpenGLES"
		"-framework Metal"
		"-framework CoreGraphics"
		"-framework GLKit"
		"-framework UIKit"
		"-framework QuartzCore"
		z
		iconv
		# Links the target library to the log library
		# included in the NDK.
		)


